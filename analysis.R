library(tidyverse)
d <- read.csv("average-rainfall-temperature-1.csv")
g = d$Temp
h <- hist(g, breaks = 10,
          col = "Azure", xlab = "Temperature", main = "Histogram"); 
xfit <- seq(min(g), max(g), length = 40) 
yfit <- dnorm(xfit, mean = mean(g), sd = sd(g)) 
yfit <- yfit * diff(h$mids[1:2]) * length(g) 

lines(xfit, yfit, col = "black", lwd = 2)



d <- d[c('Rainfallmm','Temp')]
cor.test(d$Rainfallmm,d$Temp,method = 'kendall')
cor(d$Rainfallmm,d$Temp,method = 'spearman')

